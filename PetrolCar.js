import Car from './Car.js';

class PetrolCar extends Car {
  constructor(make, model, fuelLevel) {
    super(make, model);
    this.fuelLevel = fuelLevel;
  }

  drive() {
    console.log(`Driving ${this.make} ${this.model} - Fuel Level: ${this.fuelLevel}`);
  }

  fillUp() {
    console.log(`Filling up ${this.make} ${this.model}`);
  }
}

export default PetrolCar;
